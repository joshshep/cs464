/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#ifndef _PUB_LAUNCHER_HPP
#define _PUB_LAUNCHER_HPP

#include <vector>
#include <string>
#include <iostream>
#include <thread>


// thank you https://github.com/nlohmann/json
#include "json.hpp"

using json = nlohmann::json;

#include "PubThread.hpp"

#include "SafeQueue.hpp"


class PubLauncher {
public:
    PubLauncher(int domain_id):_domain_id(domain_id) {
        loadConfigSettings();
        initThreads();
    }
    ~PubLauncher(){
        for (auto& pubThread : _pubThreads) {
            delete pubThread;
        }
        delete _participant;
        delete _positionTopic;
        delete _accidentTopic;
        delete _breakdownTopic;
        delete _backupBuses;
    }

    // launch the initialized threads. Call AFTER initThreads()
    int launchThreads() {
        std::cout << "Launching threads ..." << std::endl;
        for (auto& pubThread : _pubThreads) {
            pubThread->start();
        }
        std::cout << "Launched threads. Joining ..." << std::endl;
        for (auto& pubThread : _pubThreads) {
            pubThread->join();
        }
        std::cout << "All threads joined." << std::endl;
        return 0;
    }
private:
    //reads json file
    int loadConfigSettings(std::string fname="pub_properties.json") {
        std::ifstream ifs(fname);
        ifs >> _conf;

        
        _numInitialBackupVehicles = _conf["numInitialBackupVehicles"];
        _numRoutes = _conf["numRoutes"];
        _numVehicles = _conf["numVehicles"];
        
        return 0;
    }
    
    // set up the threads with the configurations loaded from the json file
    int initThreads() {
        _participant = new dds::domain::DomainParticipant(_domain_id);
        _positionTopic = new dds::topic::Topic<Position>(*_participant, "joshshep: PT/POS");
        _accidentTopic = new dds::topic::Topic<Accident>(*_participant, "joshshep: PT/ACC");
        _breakdownTopic = new dds::topic::Topic<Breakdown>(*_participant, "joshshep: PT/BRK");
        
        for(auto& routeConf : _conf["routes"]) {
            std::cout << "Initializing " << routeConf["name"] << " route ..." << std::endl;
            auto itBus = routeConf["vehicles"].end() - 1;
            
            // store the backup buses in a threadsafe queue
            _backupBuses = new SafeQueue<std::string>;
            for (int iBackups=0; iBackups < _conf["numInitialBackupVehicles"]; ++iBackups) {
                _backupBuses->enqueue(*itBus);
                --itBus;
            }
            // throw the rest of the buses into PubThread
            for (int ibus=0; ibus < _conf["numVehicles"]; ++ibus) {
                PubThread * pubThread = new PubThread(_participant, _positionTopic, _accidentTopic, _breakdownTopic,
                                                      *itBus, 
                                                      routeConf["name"], 
                                                      routeConf["timeBetweenStops"],
                                                      routeConf["numStops"],
                                                      _backupBuses, _conf["accidentProb"], _conf["breakdownProb"]);
                _pubThreads.push_back(pubThread);
                --itBus;
            }
        }
        return 0;
    }


    // stores the references to the PubThreads 
    std::vector<PubThread *> _pubThreads;

    // store the configuration settings loaded from the json
    json _conf;
    int _numInitialBackupVehicles;
    int _numRoutes;
    int _numVehicles;


    // domain participant. This is passed (by reference) to PubThead's
    dds::domain::DomainParticipant * _participant;
    int _domain_id;

    //topics
    dds::topic::Topic<Position> * _positionTopic;
    dds::topic::Topic<Accident> * _accidentTopic;
    dds::topic::Topic<Breakdown> * _breakdownTopic;

    // store the backup buses in a threadsafe queue
    SafeQueue<std::string> * _backupBuses;
};

#endif