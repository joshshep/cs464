

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from BusSimImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif

#ifndef cdr_type_h
#include "cdr/cdr_type.h"
#endif    

#ifndef osapi_heap_h
#include "osapi/osapi_heap.h" 
#endif
#else
#include "ndds_standalone_type.h"
#endif

#include "BusSimImpl.h"

/* ========================================================================= */
const char *Accident_cTYPENAME = "Accident";

DDS_TypeCode* Accident_c_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode Accident_c_g_tc_timestamp_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Accident_c_g_tc_route_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Accident_c_g_tc_vehicle_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode_Member Accident_c_g_tc_members[4]=
    {

        {
            (char *)"timestamp",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"route",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"vehicle",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"stopNumber",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Accident_c_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Accident", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            Accident_c_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Accident_c*/

    if (is_initialized) {
        return &Accident_c_g_tc;
    }

    Accident_c_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Accident_c_g_tc_timestamp_string;

    Accident_c_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Accident_c_g_tc_route_string;

    Accident_c_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&Accident_c_g_tc_vehicle_string;

    Accident_c_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    is_initialized = RTI_TRUE;

    return &Accident_c_g_tc;
}

RTIBool Accident_c_initialize(
    Accident_c* sample) {
    return Accident_c_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Accident_c_initialize_ex(
    Accident_c* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Accident_c_initialize_w_params(
        sample,&allocParams);

}

RTIBool Accident_c_initialize_w_params(
    Accident_c* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory){
        sample->timestamp= DDS_String_alloc ((255));
        if (sample->timestamp == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->timestamp!= NULL) { 
            sample->timestamp[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->route= DDS_String_alloc ((255));
        if (sample->route == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->route!= NULL) { 
            sample->route[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->vehicle= DDS_String_alloc ((255));
        if (sample->vehicle == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->vehicle!= NULL) { 
            sample->vehicle[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->stopNumber)) {
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

void Accident_c_finalize(
    Accident_c* sample)
{

    Accident_c_finalize_ex(sample,RTI_TRUE);
}

void Accident_c_finalize_ex(
    Accident_c* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Accident_c_finalize_w_params(
        sample,&deallocParams);
}

void Accident_c_finalize_w_params(
    Accident_c* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->timestamp != NULL) {
        DDS_String_free(sample->timestamp);
        sample->timestamp=NULL;

    }
    if (sample->route != NULL) {
        DDS_String_free(sample->route);
        sample->route=NULL;

    }
    if (sample->vehicle != NULL) {
        DDS_String_free(sample->vehicle);
        sample->vehicle=NULL;

    }

}

void Accident_c_finalize_optional_members(
    Accident_c* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Accident_c_copy(
    Accident_c* dst,
    const Accident_c* src)
{

    if (dst == NULL || src == NULL) {
        return RTI_FALSE;
    }

    if (!RTICdrType_copyStringEx (
        &dst->timestamp, src->timestamp, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->route, src->route, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->vehicle, src->vehicle, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->stopNumber, &src->stopNumber)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Accident_c' sequence class.
*/
#define T Accident_c
#define TSeq Accident_cSeq

#define T_initialize_w_params Accident_c_initialize_w_params

#define T_finalize_w_params   Accident_c_finalize_w_params
#define T_copy       Accident_c_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

/* ========================================================================= */
const char *Breakdown_cTYPENAME = "Breakdown";

DDS_TypeCode* Breakdown_c_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode Breakdown_c_g_tc_timestamp_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Breakdown_c_g_tc_route_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Breakdown_c_g_tc_vehicle_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode_Member Breakdown_c_g_tc_members[4]=
    {

        {
            (char *)"timestamp",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"route",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"vehicle",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"stopNumber",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Breakdown_c_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Breakdown", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            4, /* Number of members */
            Breakdown_c_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Breakdown_c*/

    if (is_initialized) {
        return &Breakdown_c_g_tc;
    }

    Breakdown_c_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Breakdown_c_g_tc_timestamp_string;

    Breakdown_c_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Breakdown_c_g_tc_route_string;

    Breakdown_c_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&Breakdown_c_g_tc_vehicle_string;

    Breakdown_c_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    is_initialized = RTI_TRUE;

    return &Breakdown_c_g_tc;
}

RTIBool Breakdown_c_initialize(
    Breakdown_c* sample) {
    return Breakdown_c_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Breakdown_c_initialize_ex(
    Breakdown_c* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Breakdown_c_initialize_w_params(
        sample,&allocParams);

}

RTIBool Breakdown_c_initialize_w_params(
    Breakdown_c* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory){
        sample->timestamp= DDS_String_alloc ((255));
        if (sample->timestamp == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->timestamp!= NULL) { 
            sample->timestamp[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->route= DDS_String_alloc ((255));
        if (sample->route == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->route!= NULL) { 
            sample->route[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->vehicle= DDS_String_alloc ((255));
        if (sample->vehicle == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->vehicle!= NULL) { 
            sample->vehicle[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->stopNumber)) {
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

void Breakdown_c_finalize(
    Breakdown_c* sample)
{

    Breakdown_c_finalize_ex(sample,RTI_TRUE);
}

void Breakdown_c_finalize_ex(
    Breakdown_c* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Breakdown_c_finalize_w_params(
        sample,&deallocParams);
}

void Breakdown_c_finalize_w_params(
    Breakdown_c* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->timestamp != NULL) {
        DDS_String_free(sample->timestamp);
        sample->timestamp=NULL;

    }
    if (sample->route != NULL) {
        DDS_String_free(sample->route);
        sample->route=NULL;

    }
    if (sample->vehicle != NULL) {
        DDS_String_free(sample->vehicle);
        sample->vehicle=NULL;

    }

}

void Breakdown_c_finalize_optional_members(
    Breakdown_c* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Breakdown_c_copy(
    Breakdown_c* dst,
    const Breakdown_c* src)
{

    if (dst == NULL || src == NULL) {
        return RTI_FALSE;
    }

    if (!RTICdrType_copyStringEx (
        &dst->timestamp, src->timestamp, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->route, src->route, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->vehicle, src->vehicle, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->stopNumber, &src->stopNumber)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Breakdown_c' sequence class.
*/
#define T Breakdown_c
#define TSeq Breakdown_cSeq

#define T_initialize_w_params Breakdown_c_initialize_w_params

#define T_finalize_w_params   Breakdown_c_finalize_w_params
#define T_copy       Breakdown_c_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

/* ========================================================================= */
const char *Position_cTYPENAME = "Position";

DDS_TypeCode* Position_c_get_typecode()
{
    static RTIBool is_initialized = RTI_FALSE;

    static DDS_TypeCode Position_c_g_tc_timestamp_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Position_c_g_tc_route_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Position_c_g_tc_vehicle_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode Position_c_g_tc_trafficConditions_string = DDS_INITIALIZE_STRING_TYPECODE((255));
    static DDS_TypeCode_Member Position_c_g_tc_members[8]=
    {

        {
            (char *)"timestamp",/* Member name */
            {
                0,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"route",/* Member name */
            {
                1,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"vehicle",/* Member name */
            {
                2,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"stopNumber",/* Member name */
            {
                3,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"numStops",/* Member name */
            {
                4,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"timeBetweenStops",/* Member name */
            {
                5,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"trafficConditions",/* Member name */
            {
                6,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }, 
        {
            (char *)"fillInRatio",/* Member name */
            {
                7,/* Representation ID */          
                DDS_BOOLEAN_FALSE,/* Is a pointer? */
                -1, /* Bitfield bits */
                NULL/* Member type code is assigned later */
            },
            0, /* Ignored */
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            RTI_CDR_REQUIRED_MEMBER, /* Is a key? */
            DDS_PUBLIC_MEMBER,/* Member visibility */
            1,
            NULL/* Ignored */
        }
    };

    static DDS_TypeCode Position_c_g_tc =
    {{
            DDS_TK_STRUCT,/* Kind */
            DDS_BOOLEAN_FALSE, /* Ignored */
            -1, /*Ignored*/
            (char *)"Position", /* Name */
            NULL, /* Ignored */      
            0, /* Ignored */
            0, /* Ignored */
            NULL, /* Ignored */
            8, /* Number of members */
            Position_c_g_tc_members, /* Members */
            DDS_VM_NONE  /* Ignored */         
        }}; /* Type code for Position_c*/

    if (is_initialized) {
        return &Position_c_g_tc;
    }

    Position_c_g_tc_members[0]._representation._typeCode = (RTICdrTypeCode *)&Position_c_g_tc_timestamp_string;

    Position_c_g_tc_members[1]._representation._typeCode = (RTICdrTypeCode *)&Position_c_g_tc_route_string;

    Position_c_g_tc_members[2]._representation._typeCode = (RTICdrTypeCode *)&Position_c_g_tc_vehicle_string;

    Position_c_g_tc_members[3]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    Position_c_g_tc_members[4]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    Position_c_g_tc_members[5]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_double;

    Position_c_g_tc_members[6]._representation._typeCode = (RTICdrTypeCode *)&Position_c_g_tc_trafficConditions_string;

    Position_c_g_tc_members[7]._representation._typeCode = (RTICdrTypeCode *)&DDS_g_tc_long;

    is_initialized = RTI_TRUE;

    return &Position_c_g_tc;
}

RTIBool Position_c_initialize(
    Position_c* sample) {
    return Position_c_initialize_ex(sample,RTI_TRUE,RTI_TRUE);
}

RTIBool Position_c_initialize_ex(
    Position_c* sample,RTIBool allocatePointers, RTIBool allocateMemory)
{

    struct DDS_TypeAllocationParams_t allocParams =
    DDS_TYPE_ALLOCATION_PARAMS_DEFAULT;

    allocParams.allocate_pointers =  (DDS_Boolean)allocatePointers;
    allocParams.allocate_memory = (DDS_Boolean)allocateMemory;

    return Position_c_initialize_w_params(
        sample,&allocParams);

}

RTIBool Position_c_initialize_w_params(
    Position_c* sample, const struct DDS_TypeAllocationParams_t * allocParams)
{

    if (sample == NULL) {
        return RTI_FALSE;
    }
    if (allocParams == NULL) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory){
        sample->timestamp= DDS_String_alloc ((255));
        if (sample->timestamp == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->timestamp!= NULL) { 
            sample->timestamp[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->route= DDS_String_alloc ((255));
        if (sample->route == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->route!= NULL) { 
            sample->route[0] = '\0';
        }
    }

    if (allocParams->allocate_memory){
        sample->vehicle= DDS_String_alloc ((255));
        if (sample->vehicle == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->vehicle!= NULL) { 
            sample->vehicle[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->stopNumber)) {
        return RTI_FALSE;
    }

    if (!RTICdrType_initLong(&sample->numStops)) {
        return RTI_FALSE;
    }

    if (!RTICdrType_initDouble(&sample->timeBetweenStops)) {
        return RTI_FALSE;
    }

    if (allocParams->allocate_memory){
        sample->trafficConditions= DDS_String_alloc ((255));
        if (sample->trafficConditions == NULL) {
            return RTI_FALSE;
        }

    } else {
        if (sample->trafficConditions!= NULL) { 
            sample->trafficConditions[0] = '\0';
        }
    }

    if (!RTICdrType_initLong(&sample->fillInRatio)) {
        return RTI_FALSE;
    }

    return RTI_TRUE;
}

void Position_c_finalize(
    Position_c* sample)
{

    Position_c_finalize_ex(sample,RTI_TRUE);
}

void Position_c_finalize_ex(
    Position_c* sample,RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParams =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;

    if (sample==NULL) {
        return;
    } 

    deallocParams.delete_pointers = (DDS_Boolean)deletePointers;

    Position_c_finalize_w_params(
        sample,&deallocParams);
}

void Position_c_finalize_w_params(
    Position_c* sample,const struct DDS_TypeDeallocationParams_t * deallocParams)
{

    if (sample==NULL) {
        return;
    }

    if (deallocParams == NULL) {
        return;
    }

    if (sample->timestamp != NULL) {
        DDS_String_free(sample->timestamp);
        sample->timestamp=NULL;

    }
    if (sample->route != NULL) {
        DDS_String_free(sample->route);
        sample->route=NULL;

    }
    if (sample->vehicle != NULL) {
        DDS_String_free(sample->vehicle);
        sample->vehicle=NULL;

    }

    if (sample->trafficConditions != NULL) {
        DDS_String_free(sample->trafficConditions);
        sample->trafficConditions=NULL;

    }

}

void Position_c_finalize_optional_members(
    Position_c* sample, RTIBool deletePointers)
{
    struct DDS_TypeDeallocationParams_t deallocParamsTmp =
    DDS_TYPE_DEALLOCATION_PARAMS_DEFAULT;
    struct DDS_TypeDeallocationParams_t * deallocParams =
    &deallocParamsTmp;

    if (sample==NULL) {
        return;
    } 
    if (deallocParams) {} /* To avoid warnings */

    deallocParamsTmp.delete_pointers = (DDS_Boolean)deletePointers;
    deallocParamsTmp.delete_optional_members = DDS_BOOLEAN_TRUE;

}

RTIBool Position_c_copy(
    Position_c* dst,
    const Position_c* src)
{

    if (dst == NULL || src == NULL) {
        return RTI_FALSE;
    }

    if (!RTICdrType_copyStringEx (
        &dst->timestamp, src->timestamp, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->route, src->route, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->vehicle, src->vehicle, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->stopNumber, &src->stopNumber)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->numStops, &src->numStops)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyDouble (
        &dst->timeBetweenStops, &src->timeBetweenStops)) { 
        return RTI_FALSE;
    }
    if (!RTICdrType_copyStringEx (
        &dst->trafficConditions, src->trafficConditions, 
        (255) + 1,RTI_TRUE)){
        return RTI_FALSE;
    }
    if (!RTICdrType_copyLong (
        &dst->fillInRatio, &src->fillInRatio)) { 
        return RTI_FALSE;
    }

    return RTI_TRUE;

}

/**
* <<IMPLEMENTATION>>
*
* Defines:  TSeq, T
*
* Configure and implement 'Position_c' sequence class.
*/
#define T Position_c
#define TSeq Position_cSeq

#define T_initialize_w_params Position_c_initialize_w_params

#define T_finalize_w_params   Position_c_finalize_w_params
#define T_copy       Position_c_copy

#ifndef NDDS_STANDALONE_TYPE
#include "dds_c/generic/dds_c_sequence_TSeq.gen"
#else
#include "dds_c_sequence_TSeq.gen"
#endif

#undef T_copy
#undef T_finalize_w_params

#undef T_initialize_w_params

#undef TSeq
#undef T

