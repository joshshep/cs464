/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#include <iostream>
#include <stdlib.h>



#include "PubLauncher.hpp"


int main(int argc, char *argv[]) {

    int domain_id = 0;

    if (argc >= 2) {
        domain_id = atoi(argv[1]);
    }

    // To turn on additional logging, include <rti/config/Logger.hpp> and
    // uncomment the following line:
    // rti::config::Logger::instance().verbosity(rti::config::Verbosity::STATUS_ALL);

    try {
        // use PubLauncher to launch multiple PubThreads
        PubLauncher pubLauncher(domain_id);
        pubLauncher.launchThreads();
        //publisher_main(domain_id, sample_count);
    } catch (const std::exception& ex) {
        // This will catch DDS exceptions
        std::cerr << "Exception in PubLauncher(): " << ex.what() << std::endl;
        return -1;
    }

    // RTI Connext provides a finalize_participant_factory() method
    // if you want to release memory used by the participant factory singleton.
    // Uncomment the following line to release the singleton:
    //
    // dds::domain::DomainParticipant::finalize_participant_factory();

    return 0;
}