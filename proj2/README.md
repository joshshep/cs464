# Summary

So we have two different subscribers: 
* operators (who receive everything)
* passengers (who receive everything from their route until they get on a bus)

There's one type of publisher: buses. There's also a PubLauncher which starts all the bus threads for a particular route.

PubLauncher makes one domainParticipant.
Then, PubLauncher makes 3 topics: "Position", "Accident", "Breakdown".

Each PubThread makes its own publisher and a writer for each topic.
On the publisher side, we have 
* 1 DomainParticipant
* 3 topics (one for each event type)
* 8 publishers (one for each bus)
* 24 writers (3 for each publisher)

