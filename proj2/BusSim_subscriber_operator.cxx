/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#include <algorithm>
#include <iostream>

#include <dds/sub/ddssub.hpp>
#include <dds/core/ddscore.hpp>
// Or simply include <dds/dds.hpp> 

#include <stdio.h>

#include "BusSim.hpp"

#include "BusSim_print.hpp"

class OperatorSub {
public:
    OperatorSub(int domain_id) : _domain_id(domain_id) {
        _participant = new dds::domain::DomainParticipant(_domain_id);

        //topics
        _positionTopic = new dds::topic::Topic<Position>(*_participant, "joshshep: PT/POS");
        _accidentTopic = new dds::topic::Topic<Accident>(*_participant, "joshshep: PT/ACC");
        _breakdownTopic = new dds::topic::Topic<Breakdown>(*_participant, "joshshep: PT/BRK");

        _subscriber = new dds::sub::Subscriber(*_participant);

        //readers
        _positionReader = new dds::sub::DataReader<Position>(*_subscriber, *_positionTopic);
        _accidentReader = new dds::sub::DataReader<Accident>(*_subscriber, *_accidentTopic);
        _breakdownReader = new dds::sub::DataReader<Breakdown>(*_subscriber, *_breakdownTopic);
    }
    ~OperatorSub() {
        delete _participant;

        delete _positionTopic;
        delete _accidentTopic;
        delete _breakdownTopic;

        delete _subscriber;

        delete _positionReader;
        delete _accidentReader;
        delete _breakdownReader;
        
    }
    int run() {
        line_disp_header();

        // Create a ReadCondition for any data on this reader to print the type to stdout POSITION
        dds::sub::cond::ReadCondition read_condition_position(
            *_positionReader,
            dds::sub::status::DataState::any(),
            [this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Position> samples = _positionReader->take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    line_disp_idl(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a ReadCondition for any data on this reader to print the type to stdout ACCIDENT
        dds::sub::cond::ReadCondition read_condition_accident(
            *_accidentReader,
            dds::sub::status::DataState::any(),
            [this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Accident> samples = _accidentReader->take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    line_disp_idl(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a ReadCondition for any data on this reader to print the type to stdout BREAKDOWN
        dds::sub::cond::ReadCondition read_condition_breakdown(
            *_breakdownReader,
            dds::sub::status::DataState::any(),
            [this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Breakdown> samples = _breakdownReader->take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    line_disp_idl(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a WaitSet and attach the ReadCondition's
        dds::core::cond::WaitSet waitset;
        waitset += read_condition_position;
        waitset += read_condition_accident;
        waitset += read_condition_breakdown;

        // read by the waitset indefinitely
        while (true) {
            waitset.dispatch(dds::core::Duration(4)); // Wait up to 4s each time
        }
        return 0;
    }
private:
    int _domain_id;

    // participant
    dds::domain::DomainParticipant * _participant;

    // topics
    dds::topic::Topic<Position> * _positionTopic;
    dds::topic::Topic<Accident> * _accidentTopic;
    dds::topic::Topic<Breakdown> * _breakdownTopic;

    // subscriber
    dds::sub::Subscriber * _subscriber;

    // readers
    dds::sub::DataReader<Position> * _positionReader;
    dds::sub::DataReader<Accident> * _accidentReader;
    dds::sub::DataReader<Breakdown> * _breakdownReader;
};

int main(int argc, char *argv[])
{

    int domain_id = 0;

    if (argc >= 2) {
        domain_id = atoi(argv[1]);
    }

    // To turn on additional logging, include <rti/config/Logger.hpp> and
    // uncomment the following line:
    // rti::config::Logger::instance().verbosity(rti::config::Verbosity::STATUS_ALL);

    try {
        OperatorSub opSub(domain_id);
        opSub.run();
    } catch (const std::exception& ex) {
        // This will catch DDS exceptions
        std::cerr << "Exception in OperatorSub(): " << ex.what() << std::endl;
        return -1;
    }

    // RTI Connext provides a finalize_participant_factory() method
    // if you want to release memory used by the participant factory singleton.
    // Uncomment the following line to release the singleton:
    //
    // dds::domain::DomainParticipant::finalize_participant_factory();

    return 0;
}

