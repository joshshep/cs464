/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#ifndef _PUB_THREAD_HPP
#define _PUB_THREAD_HPP

#include <dds/sub/ddssub.hpp>
#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()

#include "BusSim.hpp"

#include "BusSim_print.hpp"

#include "SafeQueue.hpp"


#include <fstream>
#include <string>
#include <iomanip>
#include <vector>
#include <thread>
#include <random>
#include <map>
#include <utility>
#include <chrono>
#include <future>

#include <stdio.h>


/*

Constants defined by the assignment description

*/
//probabilities
//#define BREAKDOWN_PROB (0.05)
//#define ACCIDENT_PROB (0.10)
#define LIGHT_TRAFFIC_PROB (0.25)
#define NORMAL_TRAFFIC_PROB (0.65)
#define HEAVY_TRAFFIC_PROB (0.10)

// in seconds
#define ACCIDENT_DELAY (10.0)
//Let a replacement bus substitute the broken one, from the stop where it reported the breakdown, after waiting the 15 second delay.
#define BREAKDOWN_REPLACEMENT_DELAY (15.0)
// Put the broken bus in the substitutes list 5 seconds after it’s replaced or 20 seconds after breaking down.
#define BREAKDOWN_SUBSTITUTE_DELAY (5.0)

// multipliers (to be multiplied by default timeBetweenStops)
#define LIGHT_TRAFFIC_MULT (0.75)
#define NORMAL_TRAFFIC_MULT (1.0)
#define HEAVY_TRAFFIC_MULT (1.5)

#define NUM_LOOPS_PER_BUS (3)

class PubThread {
public:
    PubThread(
        dds::domain::DomainParticipant * participant, 
        dds::topic::Topic<Position> *positionTopic, 
        dds::topic::Topic<Accident> *accidentTopic, 
        dds::topic::Topic<Breakdown> *breakdownTopic, 
        std::string busName, std::string routeName, double timeBetweenStops, int numStops, SafeQueue<std::string> * backupBuses, double accidentProb, double breakdownProb): 
              _participant(participant),
              _positionTopic(positionTopic),
              _accidentTopic(accidentTopic),
              _breakdownTopic(breakdownTopic),
              _busName(busName),
              _routeName(routeName),
              _timeBetweenStops(timeBetweenStops),
              _numStops(numStops),
              _backupBuses(backupBuses),
              _accidentProb(accidentProb),
              _breakdownProb(breakdownProb) {
        std::cout << "Initializing " << busName << " thread ..." << std::endl;

        // initialize the random number generators
        std::random_device rd;     // only used once to initialise (seed) engine
        _rng = std::mt19937(rd());    // random-number engine used (Mersenne-Twister in this case)
        _uni_0_100 = std::uniform_int_distribution<int>(0, 100); // guaranteed unbiased
        _uni_0_1 = std::uniform_real_distribution<>(0.0, 1.0); // guaranteed unbiased

        // Create a publisher
        _publisher = new dds::pub::Publisher(*_participant);
        // Create a subscriber
        _subscriber = new dds::sub::Subscriber(*_participant);

        // Create the DataWriters with default Qos (Publisher created in-line)
        _positionWriter = new dds::pub::DataWriter<Position>(*_publisher, *_positionTopic);
        _accidentWriter = new dds::pub::DataWriter<Accident>(*_publisher, *_accidentTopic);
        _breakdownWriter = new dds::pub::DataWriter<Breakdown>(*_publisher, *_breakdownTopic);

        _curStop = 1;
        
    }
    
    ~PubThread() {
        delete _publisher;
        delete _subscriber;

        delete _positionWriter;
        delete _accidentWriter;

    }

    // calls the main processing loop (loop()) asynchronously
    int start() {
        _thread = std::thread(&PubThread::loop, this);
        return 0;
    }

    // waits for the main loop() to join
    int join() {
        _thread.join();
        return 0;
    }

    // use the breakdownWriter to write a breakdown to its topic
    int writeBreakdown(std::string curTimestamp) {
        Breakdown breakdown;
        breakdown.timestamp(curTimestamp);
        breakdown.route(_routeName);
        breakdown.vehicle(_busName);
        breakdown.stopNumber(_curStop);

        line_disp_idl(breakdown);

        _breakdownWriter->write(breakdown);
        return 0;
    }

    // use the accidentWriter to write a breakdown to its topic
    int writeAccident(std::string curTimestamp) {
        Accident accident;
        accident.timestamp(curTimestamp);
        accident.route(_routeName);
        accident.vehicle(_busName);
        accident.stopNumber(_curStop);

        line_disp_idl(accident);        

        _accidentWriter->write(accident);
        return 0;
    }

    // use the positionWriter to write a breakdown to its topic
    int writePosition(std::string curTimestamp, int fillInRatio, double thisTimeBetweenStops, std::string trafficCond) {
        Position position;
        position.route(_routeName); //const
        position.vehicle(_busName); //const
        position.numStops(_numStops); //const
        position.stopNumber(_curStop);
        position.timestamp(curTimestamp);
        position.fillInRatio(fillInRatio);
        position.timeBetweenStops(thisTimeBetweenStops);
        position.trafficConditions(trafficCond);

        line_disp_idl(position);        

        _positionWriter->write(position);
        return 0;
    }

    // gets executed at each stop to publish updated information about Position, Accident, Breakdown
    int execStop() {
        std::string curTimestamp = getCurTimestamp();
        
        // first check if we've broken down
        
        int breakdownBool = breakdownOccurred();
        if (breakdownBool) {
            writeBreakdown(curTimestamp);
            return -1;
        }


        auto randTraffic = getRandTrafficCond();
        std::string trafficCond = randTraffic.first;
        double trafficCondMult = randTraffic.second;
        double thisTimeBetweenStops = _timeBetweenStops * trafficCondMult; //+ accidentBool * ACCIDENT_DELAY;

        if (accidentOccurred()) {
            writeAccident(curTimestamp);

            thisTimeBetweenStops += ACCIDENT_DELAY;
        }

        //debug
        //printf("%lf = %lf * %lf + %d * %lf\n", thisTimeBetweenStops, _timeBetweenStops, trafficCondMult, accidentBool, ACCIDENT_DELAY);
        writePosition(curTimestamp, getRandFillInRatio(), thisTimeBetweenStops, trafficCond);


        //std::cout << "Sleeping for " << thisTimeBetweenStops << " second(s)" << std::endl;
        rti::util::sleep(dds::core::Duration::from_secs(thisTimeBetweenStops));

        iterStopIndex();
        return 0;
    }

    // enqueue the string to the safe queue in 5 seconds
    int enqueueIn5secs(std::string oldBusName) {
        // sleep for 5 seconds
        rti::util::sleep(dds::core::Duration::from_secs(BREAKDOWN_SUBSTITUTE_DELAY));

        // enqueue
        _backupBuses->enqueue(oldBusName);
        std::cout << "Adding (previously broken) " << oldBusName << " to replacement bus list ..." << std::endl;
        return 0;
    }

    // loops through the route 3 times 
    int loop() {
        int total_stops = NUM_LOOPS_PER_BUS * _numStops;
        for (int iStop=0; iStop < total_stops; ++iStop) {
            if (execStop() == -1) {
                // breakdown

                // sleep for 15 seconds
                rti::util::sleep(dds::core::Duration::from_secs(BREAKDOWN_REPLACEMENT_DELAY));

                //reset stop counter (ie how many stops are left)
                iStop = -1;
                std::string old_busname = _busName;

                // add the bus to the replacement bus queue in 5 seconds (async)
                std::async(std::launch::async, &PubThread::enqueueIn5secs, this, old_busname);

                // get the replacement bus
                _busName = _backupBuses->dequeue();
                std::cout << "Introducing replacement bus to route: " << _busName << " ..." << std::endl;
            }
        }
        std::cout << _busName << " exhausted (3 loops)" << std::endl;
        return 0;
    }
private:
    //returns a random integer between [0,100] to be used as the fill in ratio
    int getRandFillInRatio() {
        int random_integer = _uni_0_100(_rng);
        return random_integer;
    }
    // returns random double from [0,1.0]
    double getRandProb() {
        double randFlt = _uni_0_1(_rng);
        return randFlt;
    }

    // increases the stop index unless _curStop == _numStops in which case _curStop is set to 1.
    int iterStopIndex() {
        _curStop = (_curStop % _numStops) + 1;
        return 0;
    }

    //determines if probability dictates that an accident has occurred
    bool accidentOccurred() {
        return getRandProb() < _accidentProb;
    }

    //determines if probability dictates that an accident has occurred
    bool breakdownOccurred() {
        return getRandProb() < _breakdownProb;
    }

    // according to the probabilities outlined in the project description,
    // get a random traffic condition and its associated speed multiplier
    std::pair<std::string, double> getRandTrafficCond() {
        double randProb = getRandProb();
        if (randProb < LIGHT_TRAFFIC_PROB) {
            return {"Light", LIGHT_TRAFFIC_MULT};
        } else if (randProb < LIGHT_TRAFFIC_PROB + NORMAL_TRAFFIC_PROB) {
            return {"Normal", NORMAL_TRAFFIC_MULT};
        } else {
            return {"Heavy", HEAVY_TRAFFIC_MULT};
        }
    }

    // partially edited from https://stackoverflow.com/a/35157784
    // get the current time in the format HH:MM:SS
    std::string getCurTimestamp() {
        // get current time
        auto now = std::chrono::system_clock::now();

        // convert to std::time_t in order to convert to std::tm (broken time)
        auto timer = std::chrono::system_clock::to_time_t(now);

        // convert to broken time
        std::tm bt = *std::localtime(&timer);

        std::ostringstream oss;

        oss << std::put_time(&bt, "%T"); // HH:MM:SS

        return oss.str();
    }
    // domain participant (same as PubLauncher's)
    dds::domain::DomainParticipant * _participant;

    // topics
    dds::topic::Topic<Position> * _positionTopic;
    dds::topic::Topic<Accident> * _accidentTopic;
    dds::topic::Topic<Breakdown> * _breakdownTopic;

    //private data members concerning the bus operations
    std::string _busName;
    std::string _routeName;
    double _timeBetweenStops;
    int _numStops;
    int _curStop;
    

    // since PubThread spins its own independent thread, we need to store the std::thread object
    std::thread _thread;

    //for generating a random number between 0 and 100 (inclusive)
    std::mt19937 _rng;
    std::uniform_int_distribution<int> _uni_0_100;
    std::uniform_real_distribution<> _uni_0_1;

    // writers
    dds::pub::DataWriter<Position> *_positionWriter;
    dds::pub::DataWriter<Accident> *_accidentWriter;
    dds::pub::DataWriter<Breakdown> *_breakdownWriter;

    dds::sub::Subscriber * _subscriber;
    dds::pub::Publisher * _publisher;

    //backup buses
    SafeQueue<std::string> * _backupBuses;

    //configurable probabilities
    double _accidentProb;
    double _breakdownProb;

};
#endif