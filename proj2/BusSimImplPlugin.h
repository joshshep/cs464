

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from BusSimImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef BusSimImplPlugin_1212522546_h
#define BusSimImplPlugin_1212522546_h

#include "BusSimImpl.h"

struct RTICdrStream;

#ifndef pres_typePlugin_h
#include "pres/pres_typePlugin.h"
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

#define Accident_cPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define Accident_cPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define Accident_cPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define Accident_cPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define Accident_cPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern Accident_c*
Accident_cPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern Accident_c*
Accident_cPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern Accident_c*
Accident_cPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
Accident_cPluginSupport_copy_data(
    Accident_c *out,
    const Accident_c *in);

NDDSUSERDllExport extern void 
Accident_cPluginSupport_destroy_data_w_params(
    Accident_c *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
Accident_cPluginSupport_destroy_data_ex(
    Accident_c *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
Accident_cPluginSupport_destroy_data(
    Accident_c *sample);

NDDSUSERDllExport extern void 
Accident_cPluginSupport_print_data(
    const Accident_c *sample,
    const char *desc,
    unsigned int indent);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
Accident_cPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
Accident_cPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
Accident_cPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
Accident_cPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
Accident_cPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c *out,
    const Accident_c *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const Accident_c *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Accident_cPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const Accident_c *sample); 

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Accident_cPlugin_deserialize_from_cdr_buffer(
    Accident_c *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
Accident_cPlugin_data_to_string(
    const Accident_c *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
Accident_cPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
Accident_cPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
Accident_cPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Accident_cPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
Accident_cPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const Accident_c * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
Accident_cPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
Accident_cPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Accident_cPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const Accident_c *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Accident_cPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Accident_cPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    Accident_c *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
Accident_cPlugin_new(void);

NDDSUSERDllExport extern void
Accident_cPlugin_delete(struct PRESTypePlugin *);

#define Breakdown_cPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define Breakdown_cPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define Breakdown_cPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define Breakdown_cPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define Breakdown_cPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern Breakdown_c*
Breakdown_cPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern Breakdown_c*
Breakdown_cPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern Breakdown_c*
Breakdown_cPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
Breakdown_cPluginSupport_copy_data(
    Breakdown_c *out,
    const Breakdown_c *in);

NDDSUSERDllExport extern void 
Breakdown_cPluginSupport_destroy_data_w_params(
    Breakdown_c *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
Breakdown_cPluginSupport_destroy_data_ex(
    Breakdown_c *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
Breakdown_cPluginSupport_destroy_data(
    Breakdown_c *sample);

NDDSUSERDllExport extern void 
Breakdown_cPluginSupport_print_data(
    const Breakdown_c *sample,
    const char *desc,
    unsigned int indent);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
Breakdown_cPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
Breakdown_cPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
Breakdown_cPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
Breakdown_cPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
Breakdown_cPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c *out,
    const Breakdown_c *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const Breakdown_c *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Breakdown_cPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const Breakdown_c *sample); 

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Breakdown_cPlugin_deserialize_from_cdr_buffer(
    Breakdown_c *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
Breakdown_cPlugin_data_to_string(
    const Breakdown_c *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
Breakdown_cPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
Breakdown_cPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
Breakdown_cPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Breakdown_cPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
Breakdown_cPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const Breakdown_c * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
Breakdown_cPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
Breakdown_cPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Breakdown_cPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const Breakdown_c *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Breakdown_cPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Breakdown_cPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    Breakdown_c *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
Breakdown_cPlugin_new(void);

NDDSUSERDllExport extern void
Breakdown_cPlugin_delete(struct PRESTypePlugin *);

#define Position_cPlugin_get_sample PRESTypePluginDefaultEndpointData_getSample 
#define Position_cPlugin_get_buffer PRESTypePluginDefaultEndpointData_getBuffer 
#define Position_cPlugin_return_buffer PRESTypePluginDefaultEndpointData_returnBuffer 

#define Position_cPlugin_create_sample PRESTypePluginDefaultEndpointData_createSample 
#define Position_cPlugin_destroy_sample PRESTypePluginDefaultEndpointData_deleteSample 

/* --------------------------------------------------------------------------------------
Support functions:
* -------------------------------------------------------------------------------------- */

NDDSUSERDllExport extern Position_c*
Position_cPluginSupport_create_data_w_params(
    const struct DDS_TypeAllocationParams_t * alloc_params);

NDDSUSERDllExport extern Position_c*
Position_cPluginSupport_create_data_ex(RTIBool allocate_pointers);

NDDSUSERDllExport extern Position_c*
Position_cPluginSupport_create_data(void);

NDDSUSERDllExport extern RTIBool 
Position_cPluginSupport_copy_data(
    Position_c *out,
    const Position_c *in);

NDDSUSERDllExport extern void 
Position_cPluginSupport_destroy_data_w_params(
    Position_c *sample,
    const struct DDS_TypeDeallocationParams_t * dealloc_params);

NDDSUSERDllExport extern void 
Position_cPluginSupport_destroy_data_ex(
    Position_c *sample,RTIBool deallocate_pointers);

NDDSUSERDllExport extern void 
Position_cPluginSupport_destroy_data(
    Position_c *sample);

NDDSUSERDllExport extern void 
Position_cPluginSupport_print_data(
    const Position_c *sample,
    const char *desc,
    unsigned int indent);

/* ----------------------------------------------------------------------------
Callback functions:
* ---------------------------------------------------------------------------- */

NDDSUSERDllExport extern PRESTypePluginParticipantData 
Position_cPlugin_on_participant_attached(
    void *registration_data, 
    const struct PRESTypePluginParticipantInfo *participant_info,
    RTIBool top_level_registration, 
    void *container_plugin_context,
    RTICdrTypeCode *typeCode);

NDDSUSERDllExport extern void 
Position_cPlugin_on_participant_detached(
    PRESTypePluginParticipantData participant_data);

NDDSUSERDllExport extern PRESTypePluginEndpointData 
Position_cPlugin_on_endpoint_attached(
    PRESTypePluginParticipantData participant_data,
    const struct PRESTypePluginEndpointInfo *endpoint_info,
    RTIBool top_level_registration, 
    void *container_plugin_context);

NDDSUSERDllExport extern void 
Position_cPlugin_on_endpoint_detached(
    PRESTypePluginEndpointData endpoint_data);

NDDSUSERDllExport extern void    
Position_cPlugin_return_sample(
    PRESTypePluginEndpointData endpoint_data,
    Position_c *sample,
    void *handle);    

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_copy_sample(
    PRESTypePluginEndpointData endpoint_data,
    Position_c *out,
    const Position_c *in);

/* ----------------------------------------------------------------------------
(De)Serialize functions:
* ------------------------------------------------------------------------- */

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_serialize(
    PRESTypePluginEndpointData endpoint_data,
    const Position_c *sample,
    struct RTICdrStream *stream, 
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_deserialize_sample(
    PRESTypePluginEndpointData endpoint_data,
    Position_c *sample, 
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Position_cPlugin_serialize_to_cdr_buffer(
    char * buffer,
    unsigned int * length,
    const Position_c *sample); 

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_deserialize(
    PRESTypePluginEndpointData endpoint_data,
    Position_c **sample, 
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Position_cPlugin_deserialize_from_cdr_buffer(
    Position_c *sample,
    const char * buffer,
    unsigned int length);    
NDDSUSERDllExport extern DDS_ReturnCode_t
Position_cPlugin_data_to_string(
    const Position_c *sample,
    char *str,
    DDS_UnsignedLong *str_size, 
    const struct DDS_PrintFormatProperty *property);    

NDDSUSERDllExport extern RTIBool
Position_cPlugin_skip(
    PRESTypePluginEndpointData endpoint_data,
    struct RTICdrStream *stream, 
    RTIBool skip_encapsulation,  
    RTIBool skip_sample, 
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern unsigned int 
Position_cPlugin_get_serialized_sample_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);    

NDDSUSERDllExport extern unsigned int 
Position_cPlugin_get_serialized_sample_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Position_cPlugin_get_serialized_sample_min_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int
Position_cPlugin_get_serialized_sample_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment,
    const Position_c * sample);

/* --------------------------------------------------------------------------------------
Key Management functions:
* -------------------------------------------------------------------------------------- */
NDDSUSERDllExport extern PRESTypePluginKeyKind 
Position_cPlugin_get_key_kind(void);

NDDSUSERDllExport extern unsigned int 
Position_cPlugin_get_serialized_key_max_size_ex(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool * overflow,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern unsigned int 
Position_cPlugin_get_serialized_key_max_size(
    PRESTypePluginEndpointData endpoint_data,
    RTIBool include_encapsulation,
    RTIEncapsulationId encapsulation_id,
    unsigned int current_alignment);

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_serialize_key(
    PRESTypePluginEndpointData endpoint_data,
    const Position_c *sample,
    struct RTICdrStream *stream,
    RTIBool serialize_encapsulation,
    RTIEncapsulationId encapsulation_id,
    RTIBool serialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_deserialize_key_sample(
    PRESTypePluginEndpointData endpoint_data,
    Position_c * sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool 
Position_cPlugin_deserialize_key(
    PRESTypePluginEndpointData endpoint_data,
    Position_c ** sample,
    RTIBool * drop_sample,
    struct RTICdrStream *stream,
    RTIBool deserialize_encapsulation,
    RTIBool deserialize_key,
    void *endpoint_plugin_qos);

NDDSUSERDllExport extern RTIBool
Position_cPlugin_serialized_sample_to_key(
    PRESTypePluginEndpointData endpoint_data,
    Position_c *sample,
    struct RTICdrStream *stream, 
    RTIBool deserialize_encapsulation,  
    RTIBool deserialize_key, 
    void *endpoint_plugin_qos);

/* Plugin Functions */
NDDSUSERDllExport extern struct PRESTypePlugin*
Position_cPlugin_new(void);

NDDSUSERDllExport extern void
Position_cPlugin_delete(struct PRESTypePlugin *);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* BusSimImplPlugin_1212522546_h */

