/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#include <algorithm>
#include <iostream>

#include <dds/sub/ddssub.hpp>
#include <dds/core/ddscore.hpp>
#include <rti/config/Logger.hpp>
// Or simply include <dds/dds.hpp> 

#include <stdio.h>

#include "BusSim.hpp"

#include "BusSim_print.hpp"


class Passenger {
public:
    Passenger(int domain_id, std::string routeName, int startStop, int destStop): 
              _domain_id(domain_id), _routeName(routeName), _startStop(startStop), _destStop(destStop) {
        ;
        // Create a DomainParticipant with default Qos
        _participant = new dds::domain::DomainParticipant(domain_id);
        _subscriber = new dds::sub::Subscriber(*_participant);

        // topics
        _positionTopic = new dds::topic::Topic<Position>(*_participant, "joshshep: PT/POS");
        _breakdownTopic = new dds::topic::Topic<Breakdown>(*_participant, "joshshep: PT/BRK");

        _curStop = _startStop;
    }
    ~Passenger() {
        delete _participant;
        delete _subscriber;

        delete _positionTopic;
        delete _breakdownTopic;
    }

    // assuming _on_bus == true, we follow _busName until we arrive at _destStop
    int followBus() {
        // we need the quotes around the first string because rti dds requires it
        std::vector<std::string> cft_params = {"'" + _routeName + "'", "'" + _busName + "'"};
        dds::topic::ContentFilteredTopic<Position> cftPositionTopic(
            *_positionTopic, 
            std::string("CFTposition"), 
            dds::topic::Filter("route = %0 AND vehicle = %1", cft_params)
        );
        dds::topic::ContentFilteredTopic<Breakdown> cftBreakdownTopic(
            *_breakdownTopic, 
            std::string("CFTbreakdown"), 
            dds::topic::Filter("route = %0 AND vehicle = %1", cft_params)
        );

        // Create a DataReader with default Qos (Subscriber created in-line)
        dds::sub::DataReader<Position> positionReader(*_subscriber, cftPositionTopic);
        dds::sub::DataReader<Breakdown> breakdownReader(*_subscriber, cftBreakdownTopic);

        // Create a ReadCondition for any data on this reader and associate a handler POSITION
        dds::sub::cond::ReadCondition read_condition_position(
            positionReader,
            dds::sub::status::DataState::any(),
            [&positionReader, this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Position> samples = positionReader.take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    Position position = sample.data();
                    line_disp_idl(position);
                    std::cout << "The bus we're on is at stop " << position.stopNumber() << std::endl;
                    _curStop = position.stopNumber();
                    //follow_bus(position.vehicle());
                    //operator_disp(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a ReadCondition for any data on this reader and associate a handler BREAKDOWN        
        dds::sub::cond::ReadCondition read_condition_breakdown(
            positionReader,
            dds::sub::status::DataState::any(),
            [&breakdownReader, this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Breakdown> samples = breakdownReader.take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    Breakdown breakdown = sample.data();
                    line_disp_idl(breakdown);
                    std::cout << "Our bus has BROKEN DOWN at stop " << breakdown.stopNumber() << std::endl;
                    _on_bus = false;

                    //follow_bus(position.vehicle());
                    //operator_disp(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a WaitSet and attach the ReadConditions
        dds::core::cond::WaitSet waitset;
        waitset += read_condition_position;
        waitset += read_condition_breakdown;


        while (_curStop != _destStop && _on_bus) {
            // Wait at most 4 seconds until one or more conditions are active and
            // then call the handler of the active conditions
            waitset.dispatch(dds::core::Duration(4)); // Wait up to 4s each time
        }
        //waitset.detach(read_condition_position);
        return 0;
    }

    // we are not on a bus either because we just started or because we were stranded
    // use positionReader's to determine if a bus arrives to our current stop #
    int waitForBus() {
        // we need the quotes around the first string because rti dds requires it
        std::vector<std::string> cft_params = {
            "'" + _routeName + "'", // the bus we board must be on our route
            std::to_string(_curStop), // the bus we board must be at our current stop
            std::to_string(100) // the bus we board cannot be full
        };
        dds::topic::ContentFilteredTopic<Position> cftPositionTopic(
            *_positionTopic, 
            std::string("CFTposition"), 
            dds::topic::Filter("route = %0 AND stopNumber = %1 AND fillInRatio < %2", cft_params)
        );


        // Create a DataReader with default Qos (Subscriber created in-line)
        dds::sub::DataReader<Position> positionReader(*_subscriber, cftPositionTopic);

        // Create a ReadCondition for any data on this reader and associate a handler
        dds::sub::cond::ReadCondition read_condition_position(
            positionReader,
            dds::sub::status::DataState::any(),
            [&positionReader, this]()
        {
            // Take all samples
            dds::sub::LoanedSamples<Position> samples = positionReader.take();
            for (auto sample : samples){
                if (sample.info().valid()){
                    Position position = sample.data();
                    line_disp_idl(position);
                    std::cout << "Bus at stop found! Following bus ..." << std::endl;
                    _on_bus = true;
                    _busName = position.vehicle();

                    break;
                    //follow_bus(position.vehicle());
                    //operator_disp(sample.data());
                }   
            }

        } // The LoanedSamples destructor returns the loan
        );

        // Create a WaitSet and attach the ReadCondition
        dds::core::cond::WaitSet waitset;
        waitset += read_condition_position;

        while (!_on_bus) {
            // Dispatch will call the handlers associated to the WaitSet conditions
            // when they activate
            //std::cout << "Position subscriber sleeping for 4 sec..." << std::endl;

            // Wait at most 4 seconds until one or more conditions are active and
            // then call the handler of the active conditions
            waitset.dispatch(dds::core::Duration(4)); // Wait up to 4s each time
        }
        //waitset.detach(read_condition_position);
        return 0;
    }

    // alternatively wait for a bus and ride a bus until we arrive (_curStop == _destStop)
    // Note: this isn't guaranteed to halt
    int run() {
        while (_curStop != _destStop) {
            std::cout << "Waiting for a bus at stop #" << _curStop << " ..." << std::endl;
            waitForBus();
            std::cout << "We have boarded bus: " << _busName << std::endl;
            followBus();
        }
        std::cout << "We have arrived!" << std::endl;
        return 0;
    }
private:
    // bus data private data members
    int _domain_id;
    std::string _routeName;
    int _startStop;
    int _curStop;
    int _destStop;
    bool _on_bus = false;
    std::string _busName;

    //subscriber
    dds::sub::Subscriber * _subscriber;
    
    // domain participant
    dds::domain::DomainParticipant * _participant;

    // topics
    dds::topic::Topic<Position> * _positionTopic;
    dds::topic::Topic<Breakdown> * _breakdownTopic;
};

// Notify the user how to execute the produced binary
int printHelp() {
    std::cout << "usage: BusSim_subscriber_passenger routeName startStop destStop" << std::endl;
    return 0;
}
int main(int argc, char *argv[]) {
    // parse args
    if (argc != 4) {
        std::cout << "Error: exactly 3 arguments are required" << std::endl;
        printHelp();
        return -1;
    }
    std::string routeName(argv[1]);
    int startStop = atoi(argv[2]);
    int destStop = atoi(argv[3]);

    // To turn on additional logging, include <rti/config/Logger.hpp> and
    // uncomment the following line:
    //rti::config::Logger::instance().verbosity(rti::config::Verbosity::STATUS_ALL);

    try {
        Passenger passenger(0, routeName, startStop, destStop);
        passenger.run();
        //subscriber_main(domain_id, sample_count);
    } catch (const std::exception& ex) {
        // This will catch DDS exceptions
        std::cerr << "Exception in Passenger(): " << ex.what() << std::endl;
        return -2;
    }

    // RTI Connext provides a finalize_participant_factory() method
    // if you want to release memory used by the participant factory singleton.
    // Uncomment the following line to release the singleton:
    //
    // dds::domain::DomainParticipant::finalize_participant_factory();

    return 0;
}

