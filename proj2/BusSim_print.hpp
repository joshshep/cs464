/*
Joshua Shepherd
cs 464
2018-04-27
Proj 2
*/
#ifndef _BUSSUM_PRINT_HPP
#define _BUSSUM_PRINT_HPP

#include "BusSim.hpp"
#include <stdio.h>
/*
these *_disp() functions print the provided struct in a viewer-friendly way for the operator
*/
// friendly print accident struct
int line_disp_idl(Accident a) {
    printf("%-10s %-10s %-8s          %-6d                                     %-9s\n", 
           "Accident", 
           a.route().c_str(),
           a.vehicle().c_str(),
           a.stopNumber(),
           a.timestamp().c_str());
    return 0;
}

// this is identical to operator_disp(Accident)
// friendly print breakdown struct
int line_disp_idl(Breakdown b) {
    printf("%-10s %-10s %-8s          %-6d                                     %-9s\n", 
           "Breakdown", 
           b.route().c_str(),
           b.vehicle().c_str(),
           b.stopNumber(),
           b.timestamp().c_str());
    return 0;
}

// friendly print position struct
int line_disp_idl(Position p) {
    printf("%-10s %-10s %-8s %-8s %-6d %-10d %-17.2lf %-6d %-9s\n", 
           "Position",
           p.route().c_str(),
           p.vehicle().c_str(),
           p.trafficConditions().c_str(),
           p.stopNumber(),
           p.numStops(),
           (double) p.timeBetweenStops(),
           p.fillInRatio(),
           p.timestamp().c_str());
    return 0;
}
int line_disp_header() {
    printf("%-10s %-10s %-8s %-8s %-6s %-10s %-17s %-6s %-9s\n", 
           "MessType",
           "Route",
           "Vehicle",
           "Traffic",
           "Stop#",
           "#stops",
           "TimeBetweenStops",
           "Fill%",
           "Timestamp");
    return 0;
}

#endif