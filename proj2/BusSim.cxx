

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from BusSim.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>
#include "BusSim.hpp"
#include "BusSimImplPlugin.h"

#include <rti/util/ostream_operators.hpp>

// ---- Accident: 

Accident::Accident() :
    m_stopNumber_ (0) {
}   

Accident::Accident (
    const dds::core::string& timestamp_param,
    const dds::core::string& route_param,
    const dds::core::string& vehicle_param,
    int32_t stopNumber_param)
    :
        m_timestamp_( timestamp_param ),
        m_route_( route_param ),
        m_vehicle_( vehicle_param ),
        m_stopNumber_( stopNumber_param ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Accident::Accident(Accident&& other_) OMG_NOEXCEPT  :m_timestamp_ (std::move(other_.m_timestamp_))
,
m_route_ (std::move(other_.m_route_))
,
m_vehicle_ (std::move(other_.m_vehicle_))
,
m_stopNumber_ (std::move(other_.m_stopNumber_))
{
} 

Accident& Accident::operator=(Accident&&  other_) OMG_NOEXCEPT {
    Accident tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Accident::swap(Accident& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_timestamp_, other_.m_timestamp_);
    swap(m_route_, other_.m_route_);
    swap(m_vehicle_, other_.m_vehicle_);
    swap(m_stopNumber_, other_.m_stopNumber_);
}  

bool Accident::operator == (const Accident& other_) const {
    if (m_timestamp_ != other_.m_timestamp_) {
        return false;
    }
    if (m_route_ != other_.m_route_) {
        return false;
    }
    if (m_vehicle_ != other_.m_vehicle_) {
        return false;
    }
    if (m_stopNumber_ != other_.m_stopNumber_) {
        return false;
    }
    return true;
}
bool Accident::operator != (const Accident& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
dds::core::string& Accident::timestamp() OMG_NOEXCEPT {
    return m_timestamp_;
}

const dds::core::string& Accident::timestamp() const OMG_NOEXCEPT {
    return m_timestamp_;
}

void Accident::timestamp(const dds::core::string& value) {
    m_timestamp_ = value;
}

dds::core::string& Accident::route() OMG_NOEXCEPT {
    return m_route_;
}

const dds::core::string& Accident::route() const OMG_NOEXCEPT {
    return m_route_;
}

void Accident::route(const dds::core::string& value) {
    m_route_ = value;
}

dds::core::string& Accident::vehicle() OMG_NOEXCEPT {
    return m_vehicle_;
}

const dds::core::string& Accident::vehicle() const OMG_NOEXCEPT {
    return m_vehicle_;
}

void Accident::vehicle(const dds::core::string& value) {
    m_vehicle_ = value;
}

int32_t Accident::stopNumber() const OMG_NOEXCEPT{
    return m_stopNumber_;
}

void Accident::stopNumber(int32_t value) {
    m_stopNumber_ = value;
}

std::ostream& operator << (std::ostream& o,const Accident& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "timestamp: " << sample.timestamp()<<", ";
    o << "route: " << sample.route()<<", ";
    o << "vehicle: " << sample.vehicle()<<", ";
    o << "stopNumber: " << sample.stopNumber() ;
    o <<"]";
    return o;
}

// ---- Breakdown: 

Breakdown::Breakdown() :
    m_stopNumber_ (0) {
}   

Breakdown::Breakdown (
    const dds::core::string& timestamp_param,
    const dds::core::string& route_param,
    const dds::core::string& vehicle_param,
    int32_t stopNumber_param)
    :
        m_timestamp_( timestamp_param ),
        m_route_( route_param ),
        m_vehicle_( vehicle_param ),
        m_stopNumber_( stopNumber_param ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Breakdown::Breakdown(Breakdown&& other_) OMG_NOEXCEPT  :m_timestamp_ (std::move(other_.m_timestamp_))
,
m_route_ (std::move(other_.m_route_))
,
m_vehicle_ (std::move(other_.m_vehicle_))
,
m_stopNumber_ (std::move(other_.m_stopNumber_))
{
} 

Breakdown& Breakdown::operator=(Breakdown&&  other_) OMG_NOEXCEPT {
    Breakdown tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Breakdown::swap(Breakdown& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_timestamp_, other_.m_timestamp_);
    swap(m_route_, other_.m_route_);
    swap(m_vehicle_, other_.m_vehicle_);
    swap(m_stopNumber_, other_.m_stopNumber_);
}  

bool Breakdown::operator == (const Breakdown& other_) const {
    if (m_timestamp_ != other_.m_timestamp_) {
        return false;
    }
    if (m_route_ != other_.m_route_) {
        return false;
    }
    if (m_vehicle_ != other_.m_vehicle_) {
        return false;
    }
    if (m_stopNumber_ != other_.m_stopNumber_) {
        return false;
    }
    return true;
}
bool Breakdown::operator != (const Breakdown& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
dds::core::string& Breakdown::timestamp() OMG_NOEXCEPT {
    return m_timestamp_;
}

const dds::core::string& Breakdown::timestamp() const OMG_NOEXCEPT {
    return m_timestamp_;
}

void Breakdown::timestamp(const dds::core::string& value) {
    m_timestamp_ = value;
}

dds::core::string& Breakdown::route() OMG_NOEXCEPT {
    return m_route_;
}

const dds::core::string& Breakdown::route() const OMG_NOEXCEPT {
    return m_route_;
}

void Breakdown::route(const dds::core::string& value) {
    m_route_ = value;
}

dds::core::string& Breakdown::vehicle() OMG_NOEXCEPT {
    return m_vehicle_;
}

const dds::core::string& Breakdown::vehicle() const OMG_NOEXCEPT {
    return m_vehicle_;
}

void Breakdown::vehicle(const dds::core::string& value) {
    m_vehicle_ = value;
}

int32_t Breakdown::stopNumber() const OMG_NOEXCEPT{
    return m_stopNumber_;
}

void Breakdown::stopNumber(int32_t value) {
    m_stopNumber_ = value;
}

std::ostream& operator << (std::ostream& o,const Breakdown& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "timestamp: " << sample.timestamp()<<", ";
    o << "route: " << sample.route()<<", ";
    o << "vehicle: " << sample.vehicle()<<", ";
    o << "stopNumber: " << sample.stopNumber() ;
    o <<"]";
    return o;
}

// ---- Position: 

Position::Position() :
    m_stopNumber_ (0) ,
    m_numStops_ (0) ,
    m_timeBetweenStops_ (0.0) ,
    m_fillInRatio_ (0) {
}   

Position::Position (
    const dds::core::string& timestamp_param,
    const dds::core::string& route_param,
    const dds::core::string& vehicle_param,
    int32_t stopNumber_param,
    int32_t numStops_param,
    double timeBetweenStops_param,
    const dds::core::string& trafficConditions_param,
    int32_t fillInRatio_param)
    :
        m_timestamp_( timestamp_param ),
        m_route_( route_param ),
        m_vehicle_( vehicle_param ),
        m_stopNumber_( stopNumber_param ),
        m_numStops_( numStops_param ),
        m_timeBetweenStops_( timeBetweenStops_param ),
        m_trafficConditions_( trafficConditions_param ),
        m_fillInRatio_( fillInRatio_param ) {
}

#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Position::Position(Position&& other_) OMG_NOEXCEPT  :m_timestamp_ (std::move(other_.m_timestamp_))
,
m_route_ (std::move(other_.m_route_))
,
m_vehicle_ (std::move(other_.m_vehicle_))
,
m_stopNumber_ (std::move(other_.m_stopNumber_))
,
m_numStops_ (std::move(other_.m_numStops_))
,
m_timeBetweenStops_ (std::move(other_.m_timeBetweenStops_))
,
m_trafficConditions_ (std::move(other_.m_trafficConditions_))
,
m_fillInRatio_ (std::move(other_.m_fillInRatio_))
{
} 

Position& Position::operator=(Position&&  other_) OMG_NOEXCEPT {
    Position tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Position::swap(Position& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_timestamp_, other_.m_timestamp_);
    swap(m_route_, other_.m_route_);
    swap(m_vehicle_, other_.m_vehicle_);
    swap(m_stopNumber_, other_.m_stopNumber_);
    swap(m_numStops_, other_.m_numStops_);
    swap(m_timeBetweenStops_, other_.m_timeBetweenStops_);
    swap(m_trafficConditions_, other_.m_trafficConditions_);
    swap(m_fillInRatio_, other_.m_fillInRatio_);
}  

bool Position::operator == (const Position& other_) const {
    if (m_timestamp_ != other_.m_timestamp_) {
        return false;
    }
    if (m_route_ != other_.m_route_) {
        return false;
    }
    if (m_vehicle_ != other_.m_vehicle_) {
        return false;
    }
    if (m_stopNumber_ != other_.m_stopNumber_) {
        return false;
    }
    if (m_numStops_ != other_.m_numStops_) {
        return false;
    }
    if (m_timeBetweenStops_ != other_.m_timeBetweenStops_) {
        return false;
    }
    if (m_trafficConditions_ != other_.m_trafficConditions_) {
        return false;
    }
    if (m_fillInRatio_ != other_.m_fillInRatio_) {
        return false;
    }
    return true;
}
bool Position::operator != (const Position& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
dds::core::string& Position::timestamp() OMG_NOEXCEPT {
    return m_timestamp_;
}

const dds::core::string& Position::timestamp() const OMG_NOEXCEPT {
    return m_timestamp_;
}

void Position::timestamp(const dds::core::string& value) {
    m_timestamp_ = value;
}

dds::core::string& Position::route() OMG_NOEXCEPT {
    return m_route_;
}

const dds::core::string& Position::route() const OMG_NOEXCEPT {
    return m_route_;
}

void Position::route(const dds::core::string& value) {
    m_route_ = value;
}

dds::core::string& Position::vehicle() OMG_NOEXCEPT {
    return m_vehicle_;
}

const dds::core::string& Position::vehicle() const OMG_NOEXCEPT {
    return m_vehicle_;
}

void Position::vehicle(const dds::core::string& value) {
    m_vehicle_ = value;
}

int32_t Position::stopNumber() const OMG_NOEXCEPT{
    return m_stopNumber_;
}

void Position::stopNumber(int32_t value) {
    m_stopNumber_ = value;
}

int32_t Position::numStops() const OMG_NOEXCEPT{
    return m_numStops_;
}

void Position::numStops(int32_t value) {
    m_numStops_ = value;
}

double Position::timeBetweenStops() const OMG_NOEXCEPT{
    return m_timeBetweenStops_;
}

void Position::timeBetweenStops(double value) {
    m_timeBetweenStops_ = value;
}

dds::core::string& Position::trafficConditions() OMG_NOEXCEPT {
    return m_trafficConditions_;
}

const dds::core::string& Position::trafficConditions() const OMG_NOEXCEPT {
    return m_trafficConditions_;
}

void Position::trafficConditions(const dds::core::string& value) {
    m_trafficConditions_ = value;
}

int32_t Position::fillInRatio() const OMG_NOEXCEPT{
    return m_fillInRatio_;
}

void Position::fillInRatio(int32_t value) {
    m_fillInRatio_ = value;
}

std::ostream& operator << (std::ostream& o,const Position& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "timestamp: " << sample.timestamp()<<", ";
    o << "route: " << sample.route()<<", ";
    o << "vehicle: " << sample.vehicle()<<", ";
    o << "stopNumber: " << sample.stopNumber()<<", ";
    o << "numStops: " << sample.numStops()<<", ";
    o << "timeBetweenStops: " << std::setprecision(15) <<sample.timeBetweenStops()<<", ";
    o << "trafficConditions: " << sample.trafficConditions()<<", ";
    o << "fillInRatio: " << sample.fillInRatio() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        const dds::core::xtypes::StructType& dynamic_type<Accident>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Accident_c_get_typecode())));
        }

        const dds::core::xtypes::StructType& dynamic_type<Breakdown>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Breakdown_c_get_typecode())));
        }

        const dds::core::xtypes::StructType& dynamic_type<Position>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Position_c_get_typecode())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<Accident>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Accident_cPlugin_new,
                Accident_cPlugin_delete);
        }

        void topic_type_support<Accident>::initialize_sample(Accident& sample){

            Accident_c* native_sample=reinterpret_cast<Accident_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Accident_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Accident_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Accident>::to_cdr_buffer(
            std::vector<char>& buffer, const Accident& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Accident_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Accident_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Accident_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Accident_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Accident>::from_cdr_buffer(Accident& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Accident_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Accident_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Accident from cdr buffer");
        }

        void topic_type_support<Breakdown>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Breakdown_cPlugin_new,
                Breakdown_cPlugin_delete);
        }

        void topic_type_support<Breakdown>::initialize_sample(Breakdown& sample){

            Breakdown_c* native_sample=reinterpret_cast<Breakdown_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Breakdown_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Breakdown_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Breakdown>::to_cdr_buffer(
            std::vector<char>& buffer, const Breakdown& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Breakdown_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Breakdown_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Breakdown_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Breakdown_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Breakdown>::from_cdr_buffer(Breakdown& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Breakdown_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Breakdown_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Breakdown from cdr buffer");
        }

        void topic_type_support<Position>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Position_cPlugin_new,
                Position_cPlugin_delete);
        }

        void topic_type_support<Position>::initialize_sample(Position& sample){

            Position_c* native_sample=reinterpret_cast<Position_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Position_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Position_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Position>::to_cdr_buffer(
            std::vector<char>& buffer, const Position& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Position_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Position_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Position_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Position_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Position>::from_cdr_buffer(Position& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Position_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Position_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Position from cdr buffer");
        }

    }
}  

