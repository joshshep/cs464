

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from BusSim.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef BusSim_1212522546_hpp
#define BusSim_1212522546_hpp

#include <iosfwd>
#include "BusSimImpl.h"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

class NDDSUSERDllExport Accident {

  public:
    Accident();
    Accident(
        const dds::core::string& timestamp_param,
        const dds::core::string& route_param,
        const dds::core::string& vehicle_param,
        int32_t stopNumber_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Accident (Accident&&) = default;
    Accident& operator=(Accident&&) = default;
    Accident& operator=(const Accident&) = default;
    Accident(const Accident&) = default;
    #else
    Accident(Accident&& other_) OMG_NOEXCEPT;  
    Accident& operator=(Accident&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    dds::core::string& timestamp() OMG_NOEXCEPT; 
    const dds::core::string& timestamp() const OMG_NOEXCEPT;
    void timestamp(const dds::core::string& value);

    dds::core::string& route() OMG_NOEXCEPT; 
    const dds::core::string& route() const OMG_NOEXCEPT;
    void route(const dds::core::string& value);

    dds::core::string& vehicle() OMG_NOEXCEPT; 
    const dds::core::string& vehicle() const OMG_NOEXCEPT;
    void vehicle(const dds::core::string& value);

    int32_t stopNumber() const OMG_NOEXCEPT;
    void stopNumber(int32_t value);

    bool operator == (const Accident& other_) const;
    bool operator != (const Accident& other_) const;

    void swap(Accident& other_) OMG_NOEXCEPT;

  private:

    dds::core::string m_timestamp_;
    dds::core::string m_route_;
    dds::core::string m_vehicle_;
    int32_t m_stopNumber_;

};

inline void swap(Accident& a, Accident& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const Accident& sample);

class NDDSUSERDllExport Breakdown {

  public:
    Breakdown();
    Breakdown(
        const dds::core::string& timestamp_param,
        const dds::core::string& route_param,
        const dds::core::string& vehicle_param,
        int32_t stopNumber_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Breakdown (Breakdown&&) = default;
    Breakdown& operator=(Breakdown&&) = default;
    Breakdown& operator=(const Breakdown&) = default;
    Breakdown(const Breakdown&) = default;
    #else
    Breakdown(Breakdown&& other_) OMG_NOEXCEPT;  
    Breakdown& operator=(Breakdown&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    dds::core::string& timestamp() OMG_NOEXCEPT; 
    const dds::core::string& timestamp() const OMG_NOEXCEPT;
    void timestamp(const dds::core::string& value);

    dds::core::string& route() OMG_NOEXCEPT; 
    const dds::core::string& route() const OMG_NOEXCEPT;
    void route(const dds::core::string& value);

    dds::core::string& vehicle() OMG_NOEXCEPT; 
    const dds::core::string& vehicle() const OMG_NOEXCEPT;
    void vehicle(const dds::core::string& value);

    int32_t stopNumber() const OMG_NOEXCEPT;
    void stopNumber(int32_t value);

    bool operator == (const Breakdown& other_) const;
    bool operator != (const Breakdown& other_) const;

    void swap(Breakdown& other_) OMG_NOEXCEPT;

  private:

    dds::core::string m_timestamp_;
    dds::core::string m_route_;
    dds::core::string m_vehicle_;
    int32_t m_stopNumber_;

};

inline void swap(Breakdown& a, Breakdown& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const Breakdown& sample);

class NDDSUSERDllExport Position {

  public:
    Position();
    Position(
        const dds::core::string& timestamp_param,
        const dds::core::string& route_param,
        const dds::core::string& vehicle_param,
        int32_t stopNumber_param,
        int32_t numStops_param,
        double timeBetweenStops_param,
        const dds::core::string& trafficConditions_param,
        int32_t fillInRatio_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Position (Position&&) = default;
    Position& operator=(Position&&) = default;
    Position& operator=(const Position&) = default;
    Position(const Position&) = default;
    #else
    Position(Position&& other_) OMG_NOEXCEPT;  
    Position& operator=(Position&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    dds::core::string& timestamp() OMG_NOEXCEPT; 
    const dds::core::string& timestamp() const OMG_NOEXCEPT;
    void timestamp(const dds::core::string& value);

    dds::core::string& route() OMG_NOEXCEPT; 
    const dds::core::string& route() const OMG_NOEXCEPT;
    void route(const dds::core::string& value);

    dds::core::string& vehicle() OMG_NOEXCEPT; 
    const dds::core::string& vehicle() const OMG_NOEXCEPT;
    void vehicle(const dds::core::string& value);

    int32_t stopNumber() const OMG_NOEXCEPT;
    void stopNumber(int32_t value);

    int32_t numStops() const OMG_NOEXCEPT;
    void numStops(int32_t value);

    double timeBetweenStops() const OMG_NOEXCEPT;
    void timeBetweenStops(double value);

    dds::core::string& trafficConditions() OMG_NOEXCEPT; 
    const dds::core::string& trafficConditions() const OMG_NOEXCEPT;
    void trafficConditions(const dds::core::string& value);

    int32_t fillInRatio() const OMG_NOEXCEPT;
    void fillInRatio(int32_t value);

    bool operator == (const Position& other_) const;
    bool operator != (const Position& other_) const;

    void swap(Position& other_) OMG_NOEXCEPT;

  private:

    dds::core::string m_timestamp_;
    dds::core::string m_route_;
    dds::core::string m_vehicle_;
    int32_t m_stopNumber_;
    int32_t m_numStops_;
    double m_timeBetweenStops_;
    dds::core::string m_trafficConditions_;
    int32_t m_fillInRatio_;

};

inline void swap(Position& a, Position& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const Position& sample);

namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<Accident> {
            NDDSUSERDllExport static std::string value() {
                return "Accident";
            }
        };

        template<>
        struct is_topic_type<Accident> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Accident> {

            NDDSUSERDllExport static void initialize_sample(Accident& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Accident& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Accident& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

        template<>
        struct topic_type_name<Breakdown> {
            NDDSUSERDllExport static std::string value() {
                return "Breakdown";
            }
        };

        template<>
        struct is_topic_type<Breakdown> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Breakdown> {

            NDDSUSERDllExport static void initialize_sample(Breakdown& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Breakdown& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Breakdown& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

        template<>
        struct topic_type_name<Position> {
            NDDSUSERDllExport static std::string value() {
                return "Position";
            }
        };

        template<>
        struct is_topic_type<Position> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Position> {

            NDDSUSERDllExport static void initialize_sample(Position& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Position& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Position& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

    }
}

namespace rti { 
    namespace topic {
        template<>
        struct dynamic_type<Accident> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Accident> {
            typedef Accident_c type;
        };

        template<>
        struct dynamic_type<Breakdown> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Breakdown> {
            typedef Breakdown_c type;
        };

        template<>
        struct dynamic_type<Position> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Position> {
            typedef Position_c type;
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // BusSim_1212522546_hpp

