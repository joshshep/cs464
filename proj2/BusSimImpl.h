

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from BusSimImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef BusSimImpl_1212522546_h
#define BusSimImpl_1212522546_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *Accident_cTYPENAME;

typedef struct Accident_c {

    DDS_Char *   timestamp ;
    DDS_Char *   route ;
    DDS_Char *   vehicle ;
    DDS_Long   stopNumber ;

    Accident_c() {}

} Accident_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Accident_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Accident_cSeq, Accident_c);

NDDSUSERDllExport
RTIBool Accident_c_initialize(
    Accident_c* self);

NDDSUSERDllExport
RTIBool Accident_c_initialize_ex(
    Accident_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Accident_c_initialize_w_params(
    Accident_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Accident_c_finalize(
    Accident_c* self);

NDDSUSERDllExport
void Accident_c_finalize_ex(
    Accident_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Accident_c_finalize_w_params(
    Accident_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Accident_c_finalize_optional_members(
    Accident_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Accident_c_copy(
    Accident_c* dst,
    const Accident_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

extern const char *Breakdown_cTYPENAME;

typedef struct Breakdown_c {

    DDS_Char *   timestamp ;
    DDS_Char *   route ;
    DDS_Char *   vehicle ;
    DDS_Long   stopNumber ;

    Breakdown_c() {}

} Breakdown_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Breakdown_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Breakdown_cSeq, Breakdown_c);

NDDSUSERDllExport
RTIBool Breakdown_c_initialize(
    Breakdown_c* self);

NDDSUSERDllExport
RTIBool Breakdown_c_initialize_ex(
    Breakdown_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Breakdown_c_initialize_w_params(
    Breakdown_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Breakdown_c_finalize(
    Breakdown_c* self);

NDDSUSERDllExport
void Breakdown_c_finalize_ex(
    Breakdown_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Breakdown_c_finalize_w_params(
    Breakdown_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Breakdown_c_finalize_optional_members(
    Breakdown_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Breakdown_c_copy(
    Breakdown_c* dst,
    const Breakdown_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

extern const char *Position_cTYPENAME;

typedef struct Position_c {

    DDS_Char *   timestamp ;
    DDS_Char *   route ;
    DDS_Char *   vehicle ;
    DDS_Long   stopNumber ;
    DDS_Long   numStops ;
    DDS_Double   timeBetweenStops ;
    DDS_Char *   trafficConditions ;
    DDS_Long   fillInRatio ;

    Position_c() {}

} Position_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Position_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Position_cSeq, Position_c);

NDDSUSERDllExport
RTIBool Position_c_initialize(
    Position_c* self);

NDDSUSERDllExport
RTIBool Position_c_initialize_ex(
    Position_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Position_c_initialize_w_params(
    Position_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Position_c_finalize(
    Position_c* self);

NDDSUSERDllExport
void Position_c_finalize_ex(
    Position_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Position_c_finalize_w_params(
    Position_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Position_c_finalize_optional_members(
    Position_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Position_c_copy(
    Position_c* dst,
    const Position_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* BusSimImpl */

