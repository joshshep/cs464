#ifndef RANDFRUITGEN_H
#define RANDFRUITGEN_H

#include <string>

#include <iostream>
#include <fstream> // ifstream
#include <string>
#include <vector>
#include <iterator> // istream_iterator
#include <algorithm> // copy, random_shuffle

class RandFruitGen {
public:
    /*
    RandFruitGen(std::string fname="../../fruit_names.txt") {
        // open file
        std::ifstream fstrm(fname.c_str());

        if (!fstrm.is_open()) {
            std::cout << "couldn't open '" << fname << "'." << std::endl;
        }
        
        // read file in _fruitNames
        std::copy(std::istream_iterator<std::string>(fstrm),
            std::istream_iterator<std::string>(),
            std::back_inserter(_fruitNames)
        );
        std::cout << "loaded " << _fruitNames.size() << " fruit names." << std::endl;

        // shuffle _fruitNames
        std::random_shuffle(_fruitNames.begin(), _fruitNames.end());
        _nameIdx = 0;
    }
    */
    std::string getFruitName() {
        if (_fruitNames.size() == 0) {
            return "";
        }
        int curIdx = _nameIdx;
        _nameIdx = (_nameIdx + 1) % _fruitNames.size();
        return _fruitNames[curIdx];
    }
private:
    const std::vector<std::string> _fruitNames = {
        "orange",
        "banana",
        "apple",
        "pomegranate",
        "grape",
        "strawberry",
        "blueberry",
        "blackberry",
        "mango",
        "lime",
        "watermelon"
    };
    int _nameIdx = 0;
};

std::string getCurTimef() {
    auto now = std::chrono::system_clock::now();
    auto sec = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) - sec;
    std::ostringstream oss;
    oss << sec.count() << "." << std::setfill('0') << std::setw(3) << ms.count();
    return oss.str();
}

#endif